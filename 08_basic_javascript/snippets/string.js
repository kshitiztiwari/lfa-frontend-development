var quote = 'Boromir said "One Does Not Simply Walk into Mordor"';


var longString = 
"This is a very long string which needs " +
"to wrap across multiple lines because " +
"otherwise my code is unreadable.";

var longString = "This is a very long string which needs \
to wrap across multiple lines because \
otherwise my code is unreadable.";