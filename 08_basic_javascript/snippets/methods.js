string.length;
string.charAt(1);
string.toLowerCase();
string.toUpperCase();
string.split();
string.splice();
string.substring();
string.replace(/h/i, "T");


array.length();
array.join();

array.push();
array.pop();


Math.PI;
Math.pow(2, 3);
Math.sqrt(64);
Math.abs(-4.7); 
Math.ceil(4.4);     // returns 5
Math.sin(90 * Math.PI / 180);     // returns 1 (the sine of 90 degrees)
Math.min(0, 150, 30, 20, -8, -200);  // returns -200
Math.max(0, 150, 30, 20, -8, -200);  // returns 150
Math.random(); 


new Date()
Date.now();

