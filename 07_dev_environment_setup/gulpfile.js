// Initialize gulp variables
var gulp = require('gulp'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	nunjucks = require('gulp-nunjucks-render');

// Paths 
var sassSrc = './sass/**/*.sass',
	sassWatchFiles = sassSrc,
	styleDest = './',
	mapsDest = styleDest;

gulp.task('html', function(){
	gulp.src('./html/pages/*.html')
		.pipe(nunjucks( {
			path: ['./html/templates']
		}))
		.pipe(gulp.dest('./'))
})

// Gulp SASS Task
gulp.task( 'style', function(){ 
	gulp.src( sassSrc )

	.pipe( sourcemaps.init() )

	.pipe( sass({
		outputStyle : 'expanded'
	})).on('error', sass.logError)

	.pipe( sourcemaps.write( mapsDest ) )

	.pipe( gulp.dest( styleDest ) )
});




// Define Default Task
gulp.task( 'default', [ 'style', 'html' ], function(){
	gulp.watch( sassWatchFiles, ['style']);
	
} );