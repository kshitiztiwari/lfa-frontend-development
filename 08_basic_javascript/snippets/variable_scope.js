// Variable Scope
// Example 1
var variable = "top-level";

function printVariable() {
  console.log("inside printVariable, the variable holds '" +
        variable + "'.");
}

function test() {
  var variable = "local";
  console.log("inside test, the variable holds '" + variable + "'.");
  printVariable();
}

test();



// Example 2
var fullName ="John Doe";

var obj = {
	fullName : "Edward Stew",
	prop: {
		fullName : "Colin Ihrig",
		getFullName: function(){
			return this.fullName;
		}
	}
}

console.log(obj.prop.getFullName());  // Logs: Colin Ihrig
var full_name = obj.prop.getFullName;
console.log(full_name()) // Logs: John Doe