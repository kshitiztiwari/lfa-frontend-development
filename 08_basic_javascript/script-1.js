// f(x, y) = x + y
function add(x, y){
	return x + y;
}

console.log(add( 2, 3)); // 5


function yell( string ) {
	alert(string + "!!" );
}
// yell("Hello");


// Function scope
var z = 10;

function add2(x){
	var z = 5;

	console.log(z); // 5
	return function(y){
		return x + y;
	}
}

console.log(z); 	// 10
add2(5)(4); 			// 9
console.log(z);		// 10


// DO NOT RUN THE FOLLOWINT CODE
/*
function chicken() { 
	return egg();
}

function egg() {
	return chicken(); 
}

console.log (chicken() + " came first");

*/
var fruits = ["apple", "oranges", "grapes", "banana"];


// Other ways of creating function
function add(x, y){
	return x + y;
}

var add = function(x, y){
	return x+ y;
}


// Object creation
var meow = function() {
	// console.log(this);
	console.log( this.type + " cat Meows!!");
}

var cat = {
	type: "white",
	meow: meow
}

var black_cat = {
	type: "black",
	meow: meow
}

cat.meow();

function Cat( type ){
	this.type = type;
	this.meow = function() {
		console.log( this.type + " cat Meows!!");
	}
}

var white_cat = new Cat( "White" );
var black_cat = new Cat( "Black" );



// Selectors 
var heading = document.getElementById('heading');

// heading.innerText = "Hello Javascript";
// heading.innerText = heading.innerText + " and HTML";
// heading.innerHTML = heading.innerHTML + " and <small>CSS</small>";

document.querySelectorAll('.para');
document.querySelectorAll('.para:last-child');




/*
var your_name = prompt();

var my_name = document.querySelectorAll("#heading .name");
my_name[0].innerText = your_name;
*/


// Creating long string
var longString = 
"This is a very long string which needs " +
"to wrap across multiple lines because" +
"otherwise my code is unreadable.";


var longString = "This is a very long string which needs \
to wrap across multiple lines because \
otherwise my code is unreadable.";


// Events
// 

// document.addEventListener("event_name",  function)
// 

function click_handler(event){
	alert("Clicked");
	console.log("Clicked ok");
}

var heading = document.getElementById('heading');
heading.addEventListener("click", click_handler);
// heading.onclick = click_handler;



var my_name = document.querySelectorAll(".foo .name");
var input_text = document.getElementById('input_text');
function input_change_handler(event){
	// console.log(event.keyCode);
	if(input_text.value === 'hello') {
		input_text.removeEventListener("keydown", input_change_handler);
	}
	my_name[0].innerText = input_text.value;
	console.log('Input changed');
}

// input_text.addEventListener("change", input_change_handler);
// 
// keyup
// keydown
// keypress
// 
// focus
// mouseover
// mouseout
input_text.addEventListener("keydown", input_change_handler);


// input_text.getAttribute('type');


// heading.classList
// heading.classList.add()
// heading.classList.remove();



var box = document.querySelector('.box');

box.addEventListener("click", function(){
	// box.classList.add("animate");
	box.classList.toggle("animate");
	// input_text.removeEventListener("keydown", input_change_handler);
	/*
	if(box.classList.contains("animate")){
		box.classList.remove("animate")
	} else {
		box.classList.add("animate");
	}
	*/
})







