function add(x, y){
	return x + y;
}

// Dont do this
function chicken() { 
	return egg();
}

function egg() {
	return chicken(); 
}

console.log (chicken() + " came first");


// Normal Function 
function power3(base, exponent){
	var temp = 1;
	for(var i=0; i <exponent; i++ ){
		temp = temp * base;
		console.log(i, temp);
	}

	return temp;
}


// Recursive function
function power(base, exponent) {
	if (exponent == 0){
		return 1;
	} else {
		return base * power(base, exponent - 1); 
	}
}