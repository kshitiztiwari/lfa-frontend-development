	var heading_querySelector = document.querySelector('#heading');
	var para_querySelector = document.querySelectorAll('.para');

	var heading_jquery = $('#heading');
	var para_jquery = $('.para:last-child', document);


	var para_with_context = $('.para', $('.para-1'));

	var para_1 = document.querySelectorAll('.para-1');
	para_1[0].querySelectorAll('para');


	$(para_with_context[0]).text("Replaced text");

	para_with_context.eq(1).text("Replaced text with eq() method");
	para_with_context.eq(1).html("Replaced text with <em>eq()</em> method");


	var fruits = ["apple", "oranges", "grapes", "banana"];

	for(var i=0; i<fruits.length; i++){
		fruits[i];
	}

	$.each(fruits, function(index, value){
		// console.log(fruits[index]);
		console.log(index, value);
	});

	// $.each(para_with_context, function(index, value){
	// 	$(value).text("Value inserted from each()");
	// });

	// para_with_context.each(function(index, value){
	// 	$(value).text("Value inserted from each()");
	// })

	// console.log(heading_jquery.attr('class', 'baz'));
	console.log(heading_jquery.attr('some-value', 'Heading value set'));
	// console.log(heading_jquery.attr('data-value-2', 'Heading value set'));

	console.log(heading_jquery.data('value-2'));

	var handle_dblclick = function(event){
		// alert("Heading clicked");
		$(this).toggleClass('added-class');
		// $(this).fadeOut();
	}

	heading_jquery.dblclick( handle_dblclick );
	para_with_context.click( handle_dblclick ); 

	heading_jquery.hasClass('added-class');
	heading_jquery.removeClass('added-class');
	heading_jquery.toggleClass('added-class');

	// $(document).scroll( function(event){
	// 	console.log($(document).scrollTop());

	// 	if($(document).scrollTop() > 50 ){
	// 		$('body').css("color", "red");
	// 	} else {
	// 		$('body').css("color", "inherit");
	// 	}
	// })

	
 
	// heading_jquery.on( "click", function(){
	// 	alert("Heading clicked 2");
	// })	
	// 
	// 
	
	var fetch_button = $('.fetch_data');

	fetch_button.click( function(){
		$.ajax({
		url: 'https://jsonplaceholder.typicode.com/posts/1'
	}).done( function(data){
		var my_article = $('.my-article');
		var title = my_article.find('.heading');
		var article_id = my_article.find('.id');
		var article_body = my_article.find('.article-body');

		title.text(data.title);
		article_id.text(data.id);
		article_body.text(data.body);
	})
	})


$.ajax({
		url: 'https://api.fixer.io/latest'
	}).done( function(data){
		console.log(data);


		var rates = data.rates;
		var html = '';

		$.each( rates, function(key, value){
			html += '<tr>'+
			'<td>'+ key + '</td>' +
			'<td>' + value +'</td>' +
			'</tr>'
		})

		console.log(html);
	})
	

