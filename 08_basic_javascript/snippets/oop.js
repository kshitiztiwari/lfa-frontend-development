function Cat(type){
	this.type = type;
	this.meow = function(){
		console.log( type + ' cat Meows!!');
	}
}

var whiteCat = new Cat('white');

function meow() {
	console.log(this.type + ' cat Meow!!')
}

var blackCat = {
	type: 'Black',
	meow: meow
}

blackCat.meow();